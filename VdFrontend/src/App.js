import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight, faRecordVinyl, faChartSimple, faUser } from '@fortawesome/free-solid-svg-icons';
import Webcam from 'react-webcam';
import RecordRTC from 'recordrtc';
import { BlobServiceClient, AnonymousCredential } from '@azure/storage-blob'; // Import BlobServiceClient from Azure Storage client library

function App() {
  const [recording, setRecording] = useState(false);
  const [screenStream, setScreenStream] = useState(null);
  const [mediaRecorder, setMediaRecorder] = useState(null);
  const [azureBlobClient, setAzureBlobClient] = useState(null);
  const [timer, setTimer] = useState(0);
  const [timerInterval, setTimerInterval] = useState(null);
  const [excelData, setExcelData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [videoUrl, setVideoUrl] = useState('');
  const [isRecording, setIsRecording] = useState(false);
  const [stoppedManuallyCount, setStoppedManuallyCount] = useState(0);
  const [recognizedText, setRecognizedText] = useState('');
  const [total, setTotal] = useState(0);
  const [current, setCurrent] = useState(1);
  const [commentText, setCommentText] = useState('');
  const [isTabActive, setIsTabActive] = useState(true); // Flag to track tab activity
  const [arrowsVisible, setArrowsVisible] = useState(false);
  const [startButtonDisabled, setStartButtonDisabled] = useState(false);
  const[bloburl,setbloburl] = useState('');
  const [vendorId, setVendorId] = useState('');
  const [candidateMailId, setCandidateMailId] = useState('');
  const[url,seturl] =  useState('');
  const[Candidate_id,setid]=useState('');
  const [error, setError] = useState(null);
  const [intQuestions, setIntQuestions] = useState([]);
   // State to manage start button disable

  useEffect(() => {
    // Event listener for tab visibility change
    const handleVisibilityChange = () => {
      if (document.visibilityState === 'hidden') {
        // User switched to another tab, trigger fraud detection alert
        alert('Security Alert : You are not allowed to switch tabs.');
        setIsTabActive(false);
      } else {
        setIsTabActive(true);
      }
    };

    document.addEventListener('visibilitychange', handleVisibilityChange);

    // Clean up event listener on unmount
    return () => {
      document.removeEventListener('visibilitychange', handleVisibilityChange);
    };
  }, []);

  useEffect(() => {
    // Set URL when the component mounts
    seturl(window.location.href);

    // Function to handle location change
    const handleLocationChange = () => {
      const newUrl = window.location.href;
      seturl(newUrl);
      const links = newUrl.split('/');
      setid(links[3]);
    };

    // Add event listener for popstate
    window.addEventListener('popstate', handleLocationChange);

    // Clean up the event listener
    return () => {
      window.removeEventListener('popstate', handleLocationChange);
    };
  }, []); // Empty dependency array means this effect runs only once, like componentDidMount

  useEffect(() => {
    if (url) {
      const links = url.split('/');
      setid([links[3]]);
    }
  }, [url]); // Dependency array with url to run effect when url changes

  console.log("url", url);
  console.log("id", Candidate_id);
  useEffect(() => {
    const handleInterviewQuestion = () => {
      // Check if Candidate_id is not undefined or null
      if (!Candidate_id) {
        console.error('Candidate_id is not defined:', Candidate_id);
        return;
      }
      console.log("ddd", Candidate_id);
      axios.get(`http://localhost:5000/api/get_schedule/${Candidate_id}`)
      .then(response => {
        console.log('Fetched questions:', response.data); // Log data to the console
        setIntQuestions(response.data);
        const interview_questions = response.data.map(element => element.interviewquestions)
      console.log(interview_questions, "rest");
      
      const mapped_data= interview_questions[0];
      console.log(mapped_data.length,"length");

      for (const data of mapped_data) {
        console.log(data.video_url);
      }

      

      // setVendorId(jsonData.vendor_id);
       //setCandidateMailId(mapped_data.);
       setTotal(mapped_data.length);
       
     setExcelData(mapped_data);
        //fetchData();
      })
      .catch(error => {
        console.error('Error fetching questions:', error); // Log error to the console
        setError(error);
      });
    };

    handleInterviewQuestion();
  }, [Candidate_id]);
  
  
  
  useEffect(() => {
    const initializeAzureBlobClient = async () => {
      const sasToken = 'sv=2022-11-02&ss=bfqt&srt=sco&sp=rwdlacupiytfx&se=2024-05-18T20:41:13Z&st=2024-05-14T12:41:13Z&spr=https,http&sig=fSdbx9k%2F8%2BvSu%2F5oSnFSZCO3ymDEytHiQ85JjnOodQU%3D'; // Replace with provided SAS token
      const blobServiceClient = new BlobServiceClient(`https://avatarstore0409.blob.core.windows.net/?${sasToken}`, new AnonymousCredential());
      setAzureBlobClient(blobServiceClient);
    };

    initializeAzureBlobClient();
  }, []);

  useEffect(() => {
    let timer;

    if (isRecording) {
      console.log('Recording started...');
      const startRecording = async () => {
        try {
          await axios.get('http://localhost:5000/start-recording');
          setIsRecording(true);
        } catch (error) {
          console.log('Error starting recording:', error);
        }
      };

      startRecording(); // Call the startRecording function
    }
    else if (!isRecording && stoppedManuallyCount > 0 && stoppedManuallyCount < 5) {
      console.log('Recording stopped manually but will restart...');
      const stopRecording = async () => {
        try {
          await axios.get('http://localhost:5000/stop-recording');
          setIsRecording(false);
          await getRecognizedText();
        } catch (error) {
          console.error('Error stopping recording:', error);
        }
      };

      stopRecording();
      timer = setTimeout(() => {
        setIsRecording(true);
      }, 2000);
    }
    else if (!isRecording && stoppedManuallyCount === 20) {
      console.log('Recording stopped after being manually stopped 5 times');
      console.log('Recording stopped...');
      const stopRecording = async () => {
        try {
          await axios.get('/stop-recording');
          setIsRecording(false);
          await getRecognizedText();
        } catch (error) {
          console.error('Error stopping recording:', error);
        }
      };

      stopRecording();
    }

    // Cleanup function, in case you need it
    return () => {
      clearTimeout(timer);
      // Perform any cleanup here if needed
      // For example, if you have any timers or subscriptions, clear them here
    };
  }, [isRecording, stoppedManuallyCount]);

  const getRecognizedText = async () => {
    try {
      const response = await axios.get('http://localhost:5000/get-recognized-text');
      setRecognizedText(response.data.text);
    } catch (error) {
      console.error('Error getting recognized text:', error);
    }
  };

 

  const handleClick = () => {
    console.log("data")

    if (excelData.length > 0) {
      console.log(excelData, "from exceldata");
      const currentQuestion = excelData[0];
      // Display the question text and set the video URL
      console.log(currentQuestion);
      setCommentText(excelData[0].Questions);
      sendDataToBackend(currentQuestion);
      setVideoUrl(currentQuestion.video_url);
      setIsRecording(prevIsRecording => !prevIsRecording);
    }
  };


  const handleManualStop = () => {
    excelData.shift();
    console.log(excelData, 'manual stop')
    if (excelData.length > 0) {

      if (current + 1 >= total) {
        setCurrent(total);
setArrowsVisible(false);
        // const ctrl = document.querySelector('.controls');
        // console.log(ctrl, "from query");
        // ctrl.classList.add('morph');
      } else {
        setCurrent(current + 1);
      }

      const currentQuestion = excelData[0];

      // Display the question text and set the video URL
      console.log(currentQuestion, "currentsf");
      setCommentText(excelData[0].Questions);
      sendDataToBackend(currentQuestion);
      setVideoUrl(currentQuestion.video_url);
      setIsRecording(false);
      setStoppedManuallyCount(prevCount => prevCount + 1);
      console.log(stoppedManuallyCount, 'Recording stopped manually');
    }
  };

  const data = "101";
  const sendDataToBackend = async (value) => {
    console.log(data, value, "value123");
    try {
      const response = await axios.post('http://localhost:5000/api/data', { data, value ,vendorId,candidateMailId});
      console.log('Data sent successfully:', response.data);
    } catch (error) {
      console.error('Error sending data:', error);
    }
  };

  const startRecordingAzure = async () => {
    try {
      const screenStream = await navigator.mediaDevices.getDisplayMedia({
        video: true,
        audio: {
          echoCancellation: true,
          noiseSuppression: true,
          sampleRate: 44100
        }
      });
      const recorder = new RecordRTC(screenStream, { type: 'video' });
      setMediaRecorder(recorder);
      setScreenStream(screenStream);
      recorder.startRecording();

      setRecording(true);

      // Start the timer when recording starts
      startTimer();
      handleClick();

      const videoUrlToPlay = excelData[0]["video_url"];
      console.log(excelData,videoUrlToPlay, "videoUrlToPlay")
      setVideoUrl(videoUrlToPlay);
    } catch (error) {
      console.error('Error starting screen recording:', error);
    }
  };

  const stopRecordingAzure = async () => {
    try {
      mediaRecorder.stopRecording(() => {
        const blob = mediaRecorder.getBlob();
        uploadRecordedVideo(blob);
        console.log('Screen recording stopped...');
      });
      screenStream.getTracks().forEach(track => track.stop());
      setRecording(false);

      // Stop the timer when recording stops
      stopTimer();
    } catch (error) {
      console.error('Error stopping screen recording:', error);
    }
  };

  const uploadRecordedVideo = async (blob) => {
    try {
      if (!azureBlobClient) {
        console.error('Azure Blob Service Client is not initialized.');
        return;
      }

      const containerName = 'test0904'; // Change to your container name
      const containerClient = azureBlobClient.getContainerClient(containerName);
      const blobName = `recorded_video_${generateUniqueId()}.webm`; // Unique name with ID
      const blockBlobClient = containerClient.getBlockBlobClient(blobName);

      await blockBlobClient.uploadData(blob);

      const dburl = "https://avatarstore0409.blob.core.windows.net/test0904/" + blobName;
      setbloburl(dburl);

      console.log('Recorded video uploaded successfully.');
    } catch (error) {
      console.error('Error uploading recorded video:', error);
    }
  };
  const generateUniqueId = () => {
    return Date.now().toString(36) + Math.random().toString(36).substr(2);
  };
  if (bloburl !== '') {
    (async () => {
      try {
        console.log(bloburl,"ttt")
        const response = await axios.post('http://localhost:5000/blobtomongo', {bloburl,vendorId,candidateMailId});
 
        if (response.status === 200) {
          // Process successful response (e.g., extract data)
          console.log('Data sent successfully:', response.data);
        } else {
          // Handle error response
          throw new Error(`Error sending data: ${response.statusText}`);
        }
      } catch (error) {
        console.error('Error sending data:', error);
        // Handle error here (e.g., display error message to user)
      }
    })();
  }
  // Function to start the timer
  const startTimer = () => {
    const interval = setInterval(() => {
      setTimer(prevTimer => prevTimer + 1);
    }, 1000);
    setTimerInterval(interval);
  };

  // Function to stop the timer
  const stopTimer = () => {
    clearInterval(timerInterval);
    setTimer(0); // Reset the timer
  };

  // Function to format time in HH:MM:SS format
  const formatTime = (timeInSeconds) => {
    const hours = Math.floor(timeInSeconds / 3600);
    const minutes = Math.floor((timeInSeconds % 3600) / 60);
    const seconds = timeInSeconds % 60;
    return `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`;
  };

  // Function to generate a unique name for the blob
  const handleStartRecording = () => {
    // Start recording logic here
    startRecordingAzure();
    setArrowsVisible(true);
    setStartButtonDisabled(true); // Disable the start button
    //setRecording(true);
    //startTimer();

    //sendDataToBackend();
  };

  const handleStopRecording = () => {
    // Stop recording logic here
    stopRecordingAzure();
    //setRecording(false);
    //stopTimer();
  };

  

  return (
    <div className="app">
      <div className="top-dashboard">
        <div className="logo">RubikSyslabs</div>
        <div className="user-profile">
          {/* Replace with Bootstrap user profile icon */}
          <span className="icon-user"><FontAwesomeIcon icon={faUser} /></span>
          <span>User</span>
        </div>
      </div>

      <div className='web-avatar-conatiner'>
        <div className={`Webcam-container ${recording ? 'recording' : ''}`}>
          <Webcam
            audio={true}
            mirrored={true}
            screenshotFormat="image/jpeg"
            className="webcam"
            style={{
              objectFit: "cover",
              width: "100%",
              height: "100%",
              borderRadius: "8px"
            }}
          />
        </div>

        <div className='avatar-area'>
          <div className='avatar-container'>
            {videoUrl && (
              <video autoPlay controls={false} src={videoUrl} style={{ width: "100%", height: "83%", borderRadius: "8px" }} />
            )}
            {videoUrl && (
              <div className='text-container'>
                <div className='bar-icon'>
                  <FontAwesomeIcon icon={faChartSimple} />
                </div>
                <textarea className="textarea-avatar" value={commentText} ></textarea>
              </div>
            )}
          </div>
        </div>

      </div>

      <div className="container-fluid">
        <div className="row">
          <div className="col-2 custom1">
            <div className="timer-bg">
              <span className='red-circle'><FontAwesomeIcon icon={faRecordVinyl} /></span>
              <span className='timer-font'>{formatTime(timer)}</span>
            </div>
          </div>

          <div className="col-8 d-flex button-container">
            <button type="button" className={`button-success btn btn-success ${startButtonDisabled ? 'disabled' : ''}`} onClick={handleStartRecording} disabled={startButtonDisabled}>
              Start Recording
            </button>
            {arrowsVisible && ( /* Conditionally render the arrow container */
              <div className='pagination'>
                <div className='left-arrow'>
                  <FontAwesomeIcon icon={faChevronLeft} />
                </div>

                <div className='question-number'>
                  <p> <span className="current" data-current={current}>{current}</span> of <span className="total" data-total={total}>{total}</span> Questions </p>
                </div>

                <div className='right-arrow' onClick={isRecording ? handleManualStop : handleClick}>
                  <FontAwesomeIcon icon={faChevronRight} />
                </div>
              </div>
            )}
            <button type="button" className='button-danger' onClick={handleStopRecording} disabled={!recording}>Stop Recording</button>
            
          </div>

          <div className="col-2 custom3"></div>
        </div>
      </div>
    </div>
  );
}

export default App;

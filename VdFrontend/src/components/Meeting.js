import React from 'react';
 
function Meeting({ onEndMeeting }) {
  return (
<div>
      {/* Meeting UI components */}
<button type="button" className="button-danger" onClick={onEndMeeting}>End Meeting</button>
</div>
  );
}
 
export default Meeting;
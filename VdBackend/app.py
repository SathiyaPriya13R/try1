from flask import Flask, request, jsonify
from flask_cors import CORS
import urllib.parse
from pymongo import MongoClient
import os
import threading
import wave
import pyaudio
import librosa
import soundfile as sf
import speech_recognition as sr
import logging
import json
import os


app = Flask(__name__)
CORS(app)
logging.basicConfig(level=logging.DEBUG)
global_value = None
username = os.getenv('MONGO_USERNAME', 'cbadmin')
password = os.getenv('MONGO_PASSWORD', 'cab@123')
escaped_username = urllib.parse.quote_plus(username)
escaped_password = urllib.parse.quote_plus(password)
connection_string = (
    f"mongodb+srv://{escaped_username}:{escaped_password}@atlascluster.bg29nup.mongodb.net/"
    "?retryWrites=true&w=majority&appName=AtlasCluster"
)
 
# Initialize the MongoDB client
client = MongoClient(connection_string)
db = client["CloudAndBeyond"]
collection = db["grading"]
additional_data_collection = db["blob-azure"]

connection_string1 = "mongodb+srv://CB2024:Cab123@virtualai.c4wot6c.mongodb.net/?retryWrites=true&w=majority&appName=VirtualAI"

# Initialize the MongoDB client
client1 = MongoClient(connection_string1)
db1 = client1["ViTech"]
collection1 = db1["int_schedule"]





 
@app.route('/api/post_value', methods=['POST'])
def post_value():
    global global_value
    data = request.get_json()
    global_value = data.get('value')
    # print(f'Received value: {global_value}')  # Log the received value
    # You can now use `value` as needed in your backend
    
    return jsonify({'message': 'Value received successfully', 'received_value': global_value})


   
@app.route('/api/get_schedule/<cand_id>', methods=['GET'])
def get_schedules(cand_id):
    try:
        print(cand_id)
        schedules = list(collection1.find())
        for schedule in schedules:
             schedule["_id"] = str(schedule["_id"])
        filtered_list = [item for item in schedules if item["Candidate_id"] == cand_id]
        # json_array = json.dumps(filtered_list)
        print(jsonify(filtered_list),"test12")   
            
        return filtered_list, 200
    except Exception as e:
        return jsonify({"error": str(e)}), 500

 
# Global variable to indicate whether recording is in progress
recording_in_progress = False
recording_completed = threading.Event()
global_variable = None
 
def record_audio(file_path, sample_rate=44100, channels=2, chunk_size=1024):
    global recording_in_progress
    recording_in_progress = True
 
    audio = pyaudio.PyAudio()
    stream = audio.open(format=pyaudio.paInt16,
                        channels=channels,
                        rate=sample_rate,
                        input=True,
                        frames_per_buffer=chunk_size)
 
    print(f"Recording to {file_path}...")
 
    frames = []
    try:
        while recording_in_progress:
            data = stream.read(chunk_size)
            frames.append(data)
    except Exception as e:
        print(f"Error during recording: {e}")
    finally:
        stream.stop_stream()
        stream.close()
        audio.terminate()
 
        with wave.open(file_path, 'wb') as wf:
            wf.setnchannels(channels)
            wf.setsampwidth(audio.get_sample_size(pyaudio.paInt16))
            wf.setframerate(sample_rate)
            wf.writeframes(b''.join(frames))
 
        print(f"Recording completed and file {file_path} saved.")
 
        y, sr = librosa.load(file_path, sr=sample_rate)
        y_trimmed, index = librosa.effects.trim(y)
        y_denoised = librosa.effects.preemphasis(y_trimmed)
        sf.write(file_path, y_denoised, sr, format='wav')
 
        print("Background noise removed and denoised audio saved.")
        recording_completed.set()
 
def recognize_speech(file_path):
    recognizer = sr.Recognizer()
    if not os.path.exists(file_path):
        print(f"File {file_path} not found")
        return None
 
    try:
        with sr.AudioFile(file_path) as source:
            audio_data = recognizer.record(source)
        text = recognizer.recognize_google(audio_data)
        print("Recognized Text:", text)
        return text
    except sr.UnknownValueError:
        print("Could not understand audio")
        return None
    except sr.RequestError as e:
        print(f"Could not request results from Google Speech Recognition service; {e}")
        return None
 
@app.route('/start-recording', methods=['GET'])
def start_recording():
    global recording_in_progress
    recording_completed.clear()
    if not recording_in_progress:
        recorded_audio_file = "audio.wav"
        threading.Thread(target=record_audio, args=(recorded_audio_file,)).start()
        return jsonify({"message": "Recording started."})
    else:
        return jsonify({"message": "Recording already in progress."})
 
@app.route('/stop-recording', methods=['GET'])
def stop_recording():
    global recording_in_progress
    if recording_in_progress:
        recording_in_progress = False
        recording_completed.wait()  # Wait for the recording to complete
        return jsonify({"message": "Recording stopped."})
    else:
        return jsonify({"message": "No recording in progress."})
 
@app.route('/get-recognized-text', methods=['GET'])
def get_recognized_text():
    recorded_audio_file = "audio.wav"
    recording_completed.wait()  # Ensure recording is complete
    text = recognize_speech(recorded_audio_file)
    global global_variable
    global_variable = text
    return jsonify({"text": text})
 
@app.route('/api/data', methods=['POST'])
def receive_data():
    recorded_audio_file = "audio.wav"
   
    while True:
        recording_completed.wait()  # Ensure recording is complete
        text = recognize_speech(recorded_audio_file)
       
        if text:
            data = request.get_json()
            value = data.get('value')
            #print('question',value["question"])
            vendor_id = request.json.get('vendorId')
            candidate_mail_id = request.json.get('candidateMailId')
           
            print(vendor_id,"vendor_id")
            score = grade_response(text, value)
           
            if score > 0:
                print(f"Calculated Score: {score}")
                document = {
                    'vendor_id':vendor_id,
                    'candidate_mail_id':candidate_mail_id,
                    'question':value["question"],
                    'answer':value["answer"],
                    'keywords':value["keywords"],
                    'recognizedtext':text,
                    'score': score,
                   
                    }
                result = collection.insert_one(document)
                inserted_id = result.inserted_id
                print("Data uploaded to MongoDB successfully.")
                break
       
        # Clean up and retry if no valid score was obtained
        if os.path.exists(recorded_audio_file):
            os.remove(recorded_audio_file)
            print(f"File {recorded_audio_file} deleted.")
       
        global_variable = None
        recording_completed.clear()
        threading.Thread(target=record_audio, args=(recorded_audio_file,)).start()
   
    return jsonify({'status': 'success', 'data': str(inserted_id)}), 200
 
@app.route('/blobtomongo', methods=['POST'])
def receive_bloburl():
    try:
        # Extract the JSON data from the request
        data = request.get_json()
        vendor_id = request.json.get('vendorId')
        candidate_mail_id = request.json.get('candidateMailId')
        # Check if 'bloburl' is in the received JSON
        if 'bloburl' not in data:
            return jsonify({"error": "Missing 'bloburl' field in request data"}), 400
       
        bloburl = data['bloburl']
        logging.debug(f"Received bloburl: {bloburl}")
 
        # Check if the bloburl already exists in the collection
        existing_entry = additional_data_collection.find_one({"bloburl": bloburl})
        if existing_entry:
            logging.debug(f"Bloburl already exists in the database: {bloburl}")
            return jsonify({"message": "Bloburl already exists"}), 200
 
        # Prepare the data to be inserted into MongoDB
        blobdatas = {
            'vendor_id':vendor_id,
            'candidate_mail_id':candidate_mail_id,
            "bloburl": bloburl
            }
       
        # Insert the data into the MongoDB collection
        result = additional_data_collection.insert_one(blobdatas)
        inserted_id = result.inserted_id
        logging.debug(f"Inserted document ID: {inserted_id}")
 
        # Return a success response
        return jsonify({"message": "Bloburl received successfully", "inserted_id": str(inserted_id)}), 200
 
    except Exception as e:
        logging.error(f"Error processing bloburl: {e}")
        return jsonify({"error": "Internal server error"}), 500
 
def grade_response(text, data):
    keywords = data["keywords"]
    text_lower = text.lower()
    matched_keywords = sum(1 for keyword in keywords if keyword.lower() in text_lower)
    total_keywords = len(keywords)
    marks = (matched_keywords / total_keywords) * 10
    return marks
 
if __name__ == '__main__':
    app.run(debug=True, use_reloader=False)  # Disable the auto-reloader